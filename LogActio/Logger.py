#
#   logactio  -  simple framework for doing configured action on certain
#                log file events
#
#   Copyright 2012 - 2015   David Sommerseth <dazo@eurephia.org>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation, version 2
#   of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For the avoidance of doubt the "preferred form" of this code is one which
#   is in an open unpatent encumbered format. Where cryptographic key signing
#   forms part of the process of creating an executable the information
#   including keys needed to generate an equivalently functional executable
#   are deemed to be part of the source code.
#

import syslog

LOGTYPE_FILE = 0
LOGTYPE_SYSLOG = 1
LOGTYPE_STDOUT = 2

class FileLogger(object):
    def __init__(self, logfile):
        self.__logf = open(logfile, "a+")


    def _log(self, lvl, msg):
        self.__logf.write("[%i] %s\n" % (lvl, msg))
        self.__logf.flush()


    def _close(self):
        self.__logf.close()



class SysLogger(object):
    def __init__(self, destname):
        syslog.openlog(destname, syslog.LOG_PID|syslog.LOG_NOWAIT, syslog.LOG_USER)


    def _log(self, lvl, msg):
        syslog.syslog(syslog.LOG_NOTICE, msg)


    def _close(self):
        syslog.closelog()


class StdoutLogger(object):
    def __init__(self):
        import sys
        self.__logf = sys.stdout


    def _log(self, lvl, msg):
        self.__logf.write("[%i] %s\n" % (lvl, msg))
        self.__logf.flush()


    def _close(self):
        pass


class Logger(object):
    def __init__(self, logtype, logdest, verblvl):
        self.__verblevel = verblvl
        if logtype == LOGTYPE_FILE:
            self.__logger = FileLogger(logdest)
        elif logtype == LOGTYPE_SYSLOG:
            self.__logger = SysLogger(logdest)
        elif logtype == LOGTYPE_STDOUT:
            self.__logger = StdoutLogger()

    def Log(self, lvl, msg):
        if lvl <= self.__verblevel:
            self.__logger._log(lvl, msg.rstrip())


    def Close(self):
        self.__logger._close()
