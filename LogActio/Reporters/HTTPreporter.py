#
#   logactio  -  simple framework for doing configured action on certain
#                log file events
#
#   Copyright 2012 - 2015   David Sommerseth <dazo@eurephia.org>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation, version 2
#   of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For the avoidance of doubt the "preferred form" of this code is one which
#   is in an open unpatent encumbered format. Where cryptographic key signing
#   forms part of the process of creating an executable the information
#   including keys needed to generate an equivalently functional executable
#   are deemed to be part of the source code.
#

import sys, urllib, urllib2
from LogActio import Message, ReporterQueue


class HTTPreporter(ReporterQueue.ReporterQueue):
    """Simple LogActio reporter module, sending alerts via HTTP/HTTPS

    Example configuration to be used in /etc/logactio.cfg

        [Reporter:HTTP]
        module: HTTPreporter
        method: POST
        url: http://alerts.example.com/logactio/alerts.php

    This will send reports to the given URL using POST.  If you want
    to use GET instead of POST, modify or remove the 'method' variable
    """

    def __init__(self, config, logger = None):
        if "url" not in config:
            raise Exception("HTTPreporter is not configured with a URL")

        self.__method = "method" in config and config["method"] or "GET"
        self.__url =  config["url"]
        self.__log = logger and logger or self.__logfnc

        if self.__method != "GET" and self.__method != "POST":
            raise Exception("HTTPreporter is configured with an invalid submission method: '%s'" %
                            str(self.__method))

        ReporterQueue.ReporterQueue.__init__(self,
                                             "HTTPreporter",
                                             "HTTP Reporter",
                                             self.__processqueue)

    def __logfnc(self, lvl, msg):
        print("%s" % msg)
        sys.stdout.flush()


    def __processqueue(self):
        done = False

        # Process the message queue
        while not done:
            msg = self._QueueGet()

            if( msg.MessageType() == Message.MSG_SHUTDOWN ):
                # Prepare for shutdown
                done = True

            elif( msg.MessageType() == Message.MSG_SEND ):
                m = msg.Message()
                m["priority"] = msg.Priority()
                data = urllib.urlencode(m)

                if self.__method == "GET":
                    req = urllib2.Request("%s?%s" % (self.__url, data))
                    self.__log(1, "[HTTPreporter] GET %s?%s" % (self.__url, data))
                elif self.__method == "POST":
                    req = urllib2.Request(self.__url, data)
                    self.__log(1, "[HTTPreporter] POST %s {%s}" % (self.__url, data))
                else:
                    self.__log(0, "[HTTPreporter] Unknown method '%s'" %
                               self.__method)
                res = urllib2.urlopen(req)
                self.__log(4, "[HTTPreporter] Repsonse: %s" % res.read())


    def ProcessEvent(self, logfile, prefix, msg, count, threshold):
        # Format the report message
        msg = {"prefix": prefix, "count": count, "threshold": threshold,
               "message": "|".join(msg), "logfile": logfile}

        # Queue the message for sending
        self._QueueMsg(0, msg)


def InitReporter(config, logger = None):
    return HTTPreporter(config, logger)
