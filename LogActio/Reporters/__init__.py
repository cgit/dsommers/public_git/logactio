#
#   logactio  -  simple framework for doing configured action on certain
#                log file events
#
#   Copyright 2012 - 2015   David Sommerseth <dazo@eurephia.org>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation, version 2
#   of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For the avoidance of doubt the "preferred form" of this code is one which
#   is in an open unpatent encumbered format. Where cryptographic key signing
#   forms part of the process of creating an executable the information
#   including keys needed to generate an equivalently functional executable
#   are deemed to be part of the source code.
#

import sys
from LogActio import Message, ReporterQueue


class DefaultReporter(ReporterQueue.ReporterQueue):
    def __init__(self, config, logger = None):
        self.__alertprefix = 'prefix' in config and config['prefix'] or  "---> "
        self.__log = logger
        ReporterQueue.ReporterQueue.__init__(self,
                                             "DefaultReporter",
                                             "Default stdout reporter",
                                             self.__processqueue)


    def __processqueue(self):
        done = False

        # Process the message queue
        while not done:
            msg = self._QueueGet()

            if( msg.MessageType() == Message.MSG_SHUTDOWN ):
                # Prepare for shutdown
                done = True

            elif( msg.MessageType() == Message.MSG_SEND ):
                if not self.__log:
                    print("[DefaultReporter] %s" % msg)
                    sys.stdout.flush()
                else:
                    self.__log(0, "[DefaultReporter] %s" % msg)


    def ProcessEvent(self, logfile, prefix, msg, count, threshold):
        # Format the report message
        if msg is None:
            msg = "%s [%s] %s (%s)" % (self.__alertprefix, logfile, prefix, count)
        else:
            msg = "%s [%s] %s (%s): %s" % (self.__alertprefix, logfile, prefix, count, msg)

        # Queue the message for sending
        self._QueueMsg(0, msg)

