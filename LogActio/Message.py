#
#   logactio  -  simple framework for doing configured action on certain
#                log file events
#
#   Copyright 2012 - 2015   David Sommerseth <dazo@eurephia.org>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation, version 2
#   of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For the avoidance of doubt the "preferred form" of this code is one which
#   is in an open unpatent encumbered format. Where cryptographic key signing
#   forms part of the process of creating an executable the information
#   including keys needed to generate an equivalently functional executable
#   are deemed to be part of the source code.
#

MSG_SEND = 1
MSG_SHUTDOWN = 2

class Message(object):
    def __init__(self, msgtype, prio, msg):
        self.__msgtype = msgtype
        self.__prio = prio
        self.__msg = msg

    def MessageType(self):
        return self.__msgtype

    def Priority(self):
        return self.__prio;

    def Message(self):
        return self.__msg

    def __str__(self):
        return "[Type: %i, Prio: %i] %s" % (
            self.__msgtype, self.__prio, self.__msg )

