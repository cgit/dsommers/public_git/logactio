#
#   logactio  -  simple framework for doing configured action on certain
#                log file events
#
#   Copyright 2012 - 2015   David Sommerseth <dazo@eurephia.org>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation, version 2
#   of the License.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#   For the avoidance of doubt the "preferred form" of this code is one which
#   is in an open unpatent encumbered format. Where cryptographic key signing
#   forms part of the process of creating an executable the information
#   including keys needed to generate an equivalently functional executable
#   are deemed to be part of the source code.
#

import threading, queue
import LogActio.Message as Message


class ReporterQueue(object):
    def __init__(self, qname, descr, processor):
        self.__thread = None
        self.__qname = qname
        self.__description = descr
        self.__queue = queue.Queue()

        self.__thread = threading.Thread(target=processor)


    def GetName(self):
        return self.__description


    def _Start(self):
        if not self.__thread.isAlive():
            self.__thread.start()


    def _Shutdown(self):
        self.__queue.put(Message.Message(Message.MSG_SHUTDOWN, 0, None))
        self.__queue.task_done()


    def _QueueMsg(self, prio, msg):
        self.__queue.put(Message.Message(Message.MSG_SEND, 0, msg))


    def _QueueGet(self):
        return self.__queue.get()
