.. Copyright 2012 - 2015 David Sommerseth <dazo@eurephia.org>

   This is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 2 of the License.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see
   <http://www.gnu.org/licenses/>.

.. For notes on how to document Python in RST form, see e.g.:
.. http://sphinx.pocoo.org/domains.html#the-python-domain

Starting logactio
=================

The only requirement logactio have to be able to start, is the configuration
file which declares log files to watch, what to look for and how to report
when it finds a match.  The default location for this file is
*/etc/logactio.cfg*, but this can be overridden by the command line.

For a normal production setup where the configuration file is in the default
location, this would be sufficient

.. code-block:: bash

   [user@host: ~]$ logaction -d

Otherwise, you can tweak logactio a bit.

.. code-block:: bash

   [user@host: ~]$ logactio --help
   Usage: logactio [options]

   Options:
     -h, --help            show this help message and exit
     -d, --daemon          Run as a daemon
     -p PID-FILE, --pid-file=PID-FILE
                           Put pid file of logactio in this file
     --stdout-redir=FILE   Redirect all stdout data to this file (only active
                           when running as daemon)
     -c FILE, --config=FILE
                           Configuration file for logactio (Default:
                           /etc/logactio.cfg)
     -v, --verbose         Increase the log verbosity
     -L LOGTYPE, --log-type=LOGTYPE
                           Should logging go to file or syslog? (default: syslog)
     -l LOG-FILE, --log-file=LOG-FILE
                           Filename of the log file to use
     -s SYSLOG-NAME, --syslog-id=SYSLOG-NAME
                           syslog ident to use for syslog events
     --trace               On errors, show a backtrace

If you do not have the configuration file in the standard location, you may
override this as well with *--config=/path/to/my/config*.

The only privileges logactio requires is to be able to read the configured
log files.  Logactio will never try to write to these files.  If you use
*--pid-file* logactio needs to have write access to the destination directory
for the pid file.

By default logactio will do all logging via syslog.  If you want to log to a
separate file, add the *--log-type=file* and *--log-file=/path/to/my/log.file*.
When logactio writes to syslog, they are tagged with *logactio*.  This may be
modified with the *--syslog-id* argument.  To log to stdout, use
*--log-type=stdout*

If you want to run logactio as a daemon, add the *--daemon* argument as well.

For debugging, adding *--trace* and *--stdout-redir=/path/to/logaction-stdout.log*
may be useful to understand what happens.  Also adding more *-v* arguments will
gradually increase the log verbosity.

