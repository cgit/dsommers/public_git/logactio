.. logactio documentation master file, created by
   sphinx-quickstart on Mon Sep 17 10:59:56 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Logactio - simple log file watcher framework
============================================

Logactio is a simple framework for watching log files and do certain actions
when some predefined events happens.  It's modular by design and can make use
of external reporter modules to handle the actions.

The use case for logactio is on a running server where you want to alert
users and/or sys-admins when something unexpected happens.

Simple reporter modules using HTTP/HTTPS, SMTP or AMQP/Qpid are bundled with
logactio.

Contents:

.. toctree::
   :maxdepth: 2

   configuration
   starting

