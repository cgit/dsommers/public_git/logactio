.. Copyright 2012 - 2015 David Sommerseth <dazo@eurephia.org>

   This is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 2 of the License.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see
   <http://www.gnu.org/licenses/>.

.. For notes on how to document Python in RST form, see e.g.:
.. http://sphinx.pocoo.org/domains.html#the-python-domain

Installing logactio
===================

Download and unpack the logactio source code.  From a shell, use the provided
*setup.py* utility.

.. code-block:: bash

   [root@host: ~/logactio] # python setup.py install

This takes care of installing all the needed files in the proper places.

