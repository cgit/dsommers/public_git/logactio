#!/bin/bash

{
    pushd $(git rev-parse --show-toplevel)

    # Check if setup.py version matches rpm/SPECS/logactio.spec
    PYVER=$(awk -F\" '/version =/ { print $2 }' setup.py)
    SPECVER=$(awk -F\  '/^Version:/{ print $2}' rpm/SPECS/logactio.spec)
    if [ "$PYVER" != "$SPECVER" ]; then
        echo "*** ERROR ***  Version mismatch between setup.py (v$PYVER) and logactio.spec (v$SPECVER)"
        exit 1
    fi

    python3 setup.py sdist --formats=xztar
    cp dist/logactio-*.tar.xz rpm/SOURCES
    cp init/sysv/logactio rpm/SOURCES/logactio.sysv
    cp init/systemd/logactio* rpm/SOURCES/
    rpmbuild -ba --define "_topdir $(pwd)/rpm" rpm/SPECS/logactio.spec

    popd
}
