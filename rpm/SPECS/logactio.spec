Summary: A simple log tracker which acts on certain events
Name: logactio
Version: 1.2
Release: 1
License: GPLv2
Group: System Environment/Daemons
URL: http://fedorapeople.org/cgit/dsommers/public_git/logactio.git/
Source0: https://fedorapeople.org/cgit/dsommers/public_git/%{name}.git/snapshot/%{name}-%{version}.tar.xz
Source1: logactio.sysconf
Source2: logactio.service
Source3: logactio.sysv
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch

Requires:       python(abi) >= 3.6
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%if 0%{?fedora} > 0:
BuildRequires: python-sphinx
%else
BuildRequires: python3-sphinx
%endif

%description
LogActio is a modular log tracker, which observes changes and acts
through different modules based on the event.  It is purely written
in Python and can easily be extended with specific actions.

%prep
%setup -q

%build
%py3_build
pushd docs
make text man
popd


%install
rm -rf %{buildroot}

# Install logactio code
%py3_install

# Install man pages
mkdir -p %{buildroot}/%{_mandir}/man7
install -m644 docs/build/man/%{name}.7 %{buildroot}/%{_mandir}/man7/

# Install sysv-initd/systemd related stuff
mkdir -p %{buildroot}/%{_sysconfdir}/sysconfig
install -m644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/sysconfig/%{name}
install -m644 %{SOURCE2} %{buildroot}/%{_unitdir}/

%clean
rm -rf %{buildroot}

%post
%systemd_post logactio.service

%preun
%systemd_preun logactio.service

%postun
%systemd_postun_with_restart logactio.service


%files
%defattr(-,root,root,-)
%doc COPYING docs/build/text/configuration.txt docs/build/text/starting.txt
%{_mandir}/man7/*.gz
%{_bindir}/logactio
%{python3_sitelib}/LogActio
%{python3_sitelib}/*.egg-info
%config(noreplace) /etc/sysconfig/%{name}
%{_unitdir}/%{name}.service


%changelog
* Thu Jul 30 2020 David Sommerseth <dazo@eurephia.org> - 1.2.1
- Migrate to Python 3
- Improve log messages for SMTPreporter and IPTipset
- Use a simpler systemd service unit file
* Thu Nov 12 2015 David Sommerseth <dazo@eurephia.org> - 1.1-1
- Reworked regexp match values sent to the reporter modules
- Ensure that IPTipset only uses the first regexp match as an IP address
* Fri Oct 23 2015 David Sommerseth <dazo@eurephia.org> - 1.0-1
- Updated for final v1.0 with support for both systemd and sysv distros
* Wed Dec 25 2013 David Sommerseth <dazo@eurephia.org> - 0.01-1
- Initial RPM packaging

