#!/usr/bin/python3

from distutils.sysconfig import get_python_lib
from distutils.core import setup
from os.path import join
import sys

# Get PYTHONLIB with no prefix so --prefix installs work.
PYTHONLIB = join(get_python_lib(standard_lib=1, prefix=''), 'site-packages')

version = "1.2"

setup(name="logactio",
      version=version,
      description="Simple framework for executing actions on certain log events",
      author="David Sommerseth",
      author_email="dazo@eurephia.org",
      url="http://fedorapeople.org/cgit/dsommers/public_git/logactio.git/",
      license="GPLv2 only",
      packages=["LogActio", "LogActio.Reporters"],
      scripts=["logactio"]
)
